# Home Surveillance Project
![PytonIMG](http://www.joemarzullo.com/img/python3.png) ![RaspberryIMG](https://media.giphy.com/media/fi6FhIPUfc42A/giphy.gif) ![RubyOnRailsIMG](https://logo.clearbit.com/rubyonrails.org)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)  

Things you may want to cover for local development:

1. Software:  
Rails 5.1.6  
Ruby 2.6.3  
Python 2.7.16  

2. System dependencies and Services:  
Redis server 5.0.3  
Postgres 1.1.5  
Puma 3.11.4  
Screen 4.06.02  
PyYAML 5.1

3. Configuration:  
Setup SMTP in **config/settings.yml** for email notifications.  

4. Database initialization:  
Setup your PG database in **/config/database.yml**, then:  
```rake db:create```  
```rake db:migrate```  

5. Hardware Requirements:  
- RaspberryPI 4 with 4GB RAM  
- DFRobot Gravity - digital mini PIR motion sensor  
- Grove - SHT35 temperature sensor  
- Grove - 760-1100nm digital flame sensor  
- Grove - TSL2561 digital ambient light intensity sensor  
- Raspberry Pi Camera HD v2 8MPx - original camera  
- Magnetic sensor - kontaktron CMD1423  
- IDC cable 40 PIN female 20cm  
- APC Back-UPS Connect BGE90M,120V  

6. I2C system setup:  

I2C is a very commonly used standard designed to allow chips to talk between each other.  
Since Raspberry Pi can talk I2C we can connect it to a variety of I2C capable chips and modules.  
Configure RaspberryPI by open ```sudo nano /boot/config.txt``` and change those 2 lines:  

```
dtparam=i2c1=on
dtparam=i2c_arm=on
```  

Reboot device and test I2C

```
sudo i2cdetect -y 1
```

following command allow you to see all the connected I2C devices, ex.  

![I2Cdetect](https://syngress.pl/images/HomeSurveillanceProject/pi_i2c-detect.png)

In the example above, you can see hat two I2C addresses are in use – ```0x40``` and ```0x70```  
For those who have an old Raspberry model (a 256MB Raspberry Pi Model B) test I2C with ```sudo i2cdetect -y 0``` command.  

# Project Assumptions

As a flat owner, I would like to have an early warning system to protect my apartment.  
System will guard the entrance door and reacts to movement in the main hallway.  
Opening the main home entrance door will result in an email notification.  
Temperature change of XX degrees within YY seconds results in an email notification.  
Detecting fire will immediately send email notification, message will be repeated every 30 seconds until system is disabled or destroyed.  
Detection of movement in the main hall will result in taking hall photo that will be sent by email to the apartment owner.  
Every ```one hour``` sidekiq cron should check if the services are active and working properly, use ```system check service```.  

I would like to have REST interface that:

1. Turn on and off my system  
2. Create photo and send it to desired email address  
3. Create contracton that testing the door tilt sensor service  
4. Allow to store and get temperature measurments  

I would like to start services automatically with system up, use **systemd** to create services in ```/etc/systemd/system```:  
Don't forget to enable new services ```systemctl enable service_name```  

#### PUMA
```
[Unit]
Description=Surveillance

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=/path/to/your/project
ExecStart=/bin/bash -lc 'bundle exec rails server'
TimeoutSec=30
RestartSec=15s
Restart=always
KillSignal=SIGTERM
SendSIGKILL=yes

[Install]
WantedBy=multi-user.target
```  
--
#### SIDEKIQ
```
[Unit]
Description=Sidekiq

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=/path/to/your/project
ExecStart=/bin/bash -lc 'bundle exec sidekiq'
TimeoutSec=30
RestartSec=15s
Restart=always
KillSignal=SIGTERM
SendSIGKILL=yes

[Install]
WantedBy=multi-user.target
```
--  

System should be able to run from the REST API request ```POST http://host:port/systems``` :  
```
curl -X POST http://localhost:3000/systems -H 'Content-Type: application/json' -H 'cache-control: no-cache' -d '
{
    "operating": true,
    "motion_interval": 5,
    "camera_status": true,
    "mail_notification": true,
    "sms_notification": true,
    "door_sensor": true,
    "temperature_sensor": true,
    "temperature_check_interval": 10
}'
```  

I should be able to check status of the OS services pre-defined from the application configuration.  
All inactive services should be notified by email message, in the form of a numbered list.  
Read information directly from ``systemctl`` response :)
```
curl -X GET http://localhost:3000/services -H 'Content-Type: application/json'

# response
{
    "services": {
        "surveillance": {
            "status": "active"
        },
        "sidekiq": {
            "status": "active"
        }
    }
}
```

With application start system create automatically ``detached screens`` running python scripts inside.  
Try to handle each hardware from a one separate python script.  
Python scripts communicate with **RAILS REST API** to perform predetermined action.

# Functionality check  
You can check all detached screens with ``screen -list`` command.  
You can check if the application is listening on the selected port with ``sudo lsof -wni tcp:3000`` command.  

# Table of endpoints

``SYSTEM``

|Method          |Endpoint                |Note                    |
|----------------|------------------------|------------------------|
|POST			       |`/systems`            	|Initialize application  |
|GET             |`/systems/:id`          |Get System Object       |
|GET			       |`/systems`            	|Get System Collection   |
|GET             |`/services`            	|Get OS Service Status   |

``CONTRACTON``

|Method          |Endpoint                |Note                       |
|----------------|------------------------|---------------------------|
|POST			       |`/contractons`          |Create Contracton Object   |
|GET             |`/contractons/:id`      |Get Contracton Object      |
|GET			       |`/contractons`          |Get Contracton Collection  |

``CAMERA``

|Method          |Endpoint                |Note                   |
|----------------|------------------------|-----------------------|
|POST			       |`/cameras`            	|Create Camera Object   |
|GET             |`/cameras/:id`          |Get Camera Object      |
|GET             |`/cameras`              |Get Camera Collection  |

``TEMPERATURE``

|Method          |Endpoint                |Note                       |
|----------------|------------------------|---------------------------|
|POST			       |`/temperatures`         |Create Temperature Object  |

# Hardware GPIO Connections

![RPIgpio](https://syngress.pl/images/rpi_gpio.jpg)  

1. Opening sensor MCP23017 (GND Pin 09 || Pin 16 GPIO23)  
2. DFRobot Gravity Pyroelectric Infrared Motion Detector (VCC => 5V Pin 02 || GND => Pin 06 || OUT => Pin12)
3. SHT35 temperature sensor (VCC => 3.3v Pin1 || SDA => GPIO2 Pin3 || SCL => GPIO3 Pin5 || GND => Pin14)  

# API Documentation

Check out API documentations in a fast, organized, and searchable interface [Click Here](https://syngress.pl/apidoc/api_doc_surveillance.html)

**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [Syngress Network]: <https://syngress.pl>
