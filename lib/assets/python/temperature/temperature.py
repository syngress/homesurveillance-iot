#!/usr/bin/env python
# -*- coding: utf-8 -*-
import smbus
import time
import sys
import os
import os.path
import yaml
import requests
import json
from decimal import Decimal
with open('config/settings.yml', 'r') as f:
    config = yaml.safe_load(f)

# Assign Vars
bus = smbus.SMBus(1)
temperatureArray = []
celsius_threshold = config["temperature_sensor"]["celsius_threshold"]
fahrenheit_threshold = config["temperature_sensor"]["fahrenheit_threshold"]
email_notification = config["temperature_sensor"]["notification"]
mail_password = config["python_mail_config"]["password"]
mail_login = config["python_mail_config"]["login"]
mail_smtp = config["python_mail_config"]["smtp"]
recipients = config["python_mail_config"]["recipients"]

# Get temperature check interval
def temperature_check_interval():
    url = 'http://127.0.0.1:3000/systems'
    headers = {'Content-Type': 'application/json'}
    params = {'page': 1, 'per_page': 1, 'sort_order': 'desc'}
    raw_data = requests.get(url, params = params, headers = headers)
    json_acceptable_string = raw_data.text.replace("'", "\"")
    json_data = json.loads(json_acceptable_string)
    return  json_data['data'][0]['temperature_check_interval']

temp_check_interval = temperature_check_interval()

# Function check temperature changes every one sec and send mail alert if temperature is to high
def check_temperature_change(temp_c, temp_f, humidity):
    global temperatureArray
    temperatureArray.append([temp_c, temp_f])
    if len(temperatureArray) is temp_check_interval:
        temperature_celsius_exceeded = celsius_value_diff(temp_c, temperatureArray[::len(temperatureArray)-0][0])
        temperature_fahrenheit_exceeded = fahrenheit_value_diff(temp_f, temperatureArray[::len(temperatureArray)-0][0])
        if temperature_celsius_exceeded or temperature_fahrenheit_exceeded and email_notification == True:
            send_email(temp_c, temp_f, humidity)
        else:
            print('\n Temperature ' + str(temperatureArray[::len(temperatureArray)-0][0][0]) + '*C measured ' + str(temp_check_interval) + ' seconds ago is now ' + str(temperatureArray[::len(temperatureArray)-1][-1][0]) + ' which is  ' + str(temperature_parcentage(temp_c)) + '\n')
        temperatureArray = [] # reset counter and start again
    else:
        print('sec no: ' + str(len(temperatureArray)) + ' out of ' + str(temp_check_interval))

# Function check if actual temperature is above threshold in celsius
def celsius_value_diff(actual_temp_c, primary_measure_temp_c):
    temp_value = int(float(actual_temp_c)) - int(float(primary_measure_temp_c[0]))
    temp_value > celsius_threshold

# Function check if actual temperature is above threshold in fahrenheit
def fahrenheit_value_diff(actual_temp_f, primary_measure_temp_f):
    temp_value = int(float(actual_temp_f)) - int(float(primary_measure_temp_f[1]))
    temp_value > fahrenheit_threshold

# Function send email alert
def send_email(temp_c, temp_f, humidity):
    # send email message
    url = 'http://127.0.0.1:3000/temperatures'
    payload = { 'temperature_celsius': temp_c, 'temperature_fahrenheit': temp_f, 'humidity': humidity, 'notification': email_notification }
    headers = {'content-type': 'application/json'}
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    time.sleep(5.0)

# Calculate temperature parcentage from 0 to max set from configuration
def temperature_parcentage(actual_temperature):
    actual_temp = float(actual_temperature)
    threshold = float(celsius_threshold)
    maximum_temperature = actual_temp + threshold
    parcentage = (actual_temp/maximum_temperature) * 100
    return '{0:.2f}%'.format(parcentage)

# Loop and get values from sensor every one second
try:
    while True:
        # check if the I2C port is enabled by using -> ls /dev/*i2c*
        # check if 0x45 will be returned from -> i2cdetect -y 1
        bus.write_i2c_block_data(0x45, 0x2C, [0x06])
        data = bus.read_i2c_block_data(0x45, 0x00, 6)
        # Convert data
        temp = data[0] * 256 + data[1]
        cTemp = -45 + (175 * temp / 65535.0)
        fTemp = -49 + (315 * temp / 65535.0)
        humidity = 100 * (data[3] * 256 + data[4]) / 65535.0
        # Return data readable by ruby
        sensor_values = {
            "temp_c": "%.2f" %cTemp,
            "temp_f": "%.2f" %fTemp,
            "hum": "%.2f" %humidity
        }
        print(sensor_values)
        check_temperature_change("%.2f" %cTemp, "%.2f" %fTemp, "%.2f" %humidity)
        # get values with one second interval
        time.sleep(1)
except KeyboardInterrupt:
    sys.exit()
