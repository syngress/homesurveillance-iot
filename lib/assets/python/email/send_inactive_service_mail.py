#!/usr/bin/env python
# -*- coding: utf-8 -*-
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP
import smtplib
import sys
import time
import os
import os.path
import yaml
with open('config/settings.local.yml', 'r') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

mail_password = config["python_mail_config"]["password"]
mail_login = config["python_mail_config"]["login"]
mail_smtp = config["python_mail_config"]["smtp"]
recipients = config["python_mail_config"]["recipients"]

emaillist = [elem.strip().split(',') for elem in recipients]
msg = MIMEMultipart()
msg['Subject'] = 'Check status of defined systemd services: '+time.strftime('%Y-%m-%d %H:%M:%S')
msg['From'] = 'pclabacc@gmail.com'
msg['Reply-to'] = 'pclabacc@gmail.com'

msg.preamble = 'Multipart massage.\n'

print sys.argv[1:]

if sys.argv[1:] == ['[]']:
    part = MIMEText('All defined services are active')
else:
    inactive_services = "\n".join(["{0}. ""{1}".format(i+1, eachArg.strip('[]')) for i, eachArg in enumerate(sys.argv[1:])])
    part = MIMEText('The following services are not active: \n' + str(inactive_services).strip('[]'))

msg.attach(part)

server = smtplib.SMTP(mail_smtp)
server.ehlo()
server.starttls()
server.login(mail_login, mail_password)

server.sendmail(msg['From'], emaillist , msg.as_string())
