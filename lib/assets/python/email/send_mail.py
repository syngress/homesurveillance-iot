#!/usr/bin/env python
# -*- coding: utf-8 -*-
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP
import smtplib
import sys
import time
import os
import os.path
import yaml
with open('config/settings.local.yml', 'r') as f:
    config = yaml.load(f)

mail_password = config["python_mail_config"]["password"]
mail_login = config["python_mail_config"]["login"]
mail_smtp = config["python_mail_config"]["smtp"]
recipients = config["python_mail_config"]["recipients"]

emaillist = [elem.strip().split(',') for elem in recipients]
msg = MIMEMultipart()
msg['Subject'] = 'Motion Camera Photo Attachment - '+time.strftime('%Y-%m-%d %H:%M:%S') + ' - Home Address'
msg['From'] = 'pclabacc@gmail.com'
msg['Reply-to'] = 'pclabacc@gmail.com'

msg.preamble = 'Multipart massage.\n'

part = MIMEText("Alert! System detect suspicious movement, destination: small room.")
msg.attach(part)

part = MIMEApplication(open(str(sys.argv[1]),"rb").read())
part.add_header('Content-Disposition', 'attachment', filename=str(sys.argv[1]))
msg.attach(part)

server = smtplib.SMTP(mail_smtp)
server.ehlo()
server.starttls()
server.login(mail_login, mail_password)

server.sendmail(msg['From'], emaillist , msg.as_string())
