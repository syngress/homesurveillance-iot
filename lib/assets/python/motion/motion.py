#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import signal
import requests
import json
from time import sleep
from datetime import datetime

# Pin that D1 is connected to
PIR = 26 # On-board pin number 7 (GPIO04)

# Tell the Pi we're going to use it's numbering system
val   = False
GPIO.setmode(GPIO.BOARD) # Change this if using GPIO numbering
GPIO.setup(PIR, GPIO.IN) # Set PIR as input

# Variables that ensure we don't stream "Motion Detected" or "No Motion" twice !
thereIsMotion = False
thereIsNoMotion = False
i = 0

# Get motion interval
def motion_time_interval():
    url = 'http://127.0.0.1:3000/systems'
    headers = {'Content-Type': 'application/json'}
    params = {'page': 1, 'per_page': 1, 'sort_order': 'desc'}
    raw_data = requests.get(url, params = params, headers = headers)
    json_acceptable_string = raw_data.text.replace("'", "\"")
    json_data = json.loads(json_acceptable_string)
    return  json_data['data'][0]['motion_interval']
# Loop
try:
    while True:
        # If the motion sensor pulls high (detects motion):
        val = GPIO.input(PIR) # read input value
        if (val == True):
            i = i + 1
            if (i%2 == 0):
                print "Warning - Motion detected sec " + `i`
                time.sleep(1.0)
            else:
                print "Warning - Motion detected sec" + `i`
                time.sleep(1.0)
                if i > motion_time_interval():  # 3 means 5 seconds of motion detection
                    if not thereIsMotion:
                        # Send Email
                        url = 'http://127.0.0.1:3000/cameras'
                        payload = { 'width': 1024, 'height': 768, 'quality': 75 }
                        headers = {'content-type': 'application/json'}
                        r = requests.post(url, data=json.dumps(payload), headers=headers)
                        time.sleep(3.0)
                        thereIsMotion = True
                        thereIsNoMotion = False
                    else:
                        # Pause the script for 1 second
                        time.sleep(1.0)
        else:
            # No motion no problem
            print "No motion detected"
            if i == 0:
                i = 0
            else:
                i = i -1
                print "System CoolDown sec " + `i`
            if not thereIsNoMotion:
                thereIsNoMotion = True
                thereIsMotion = False
            else:
                # Pause the script for 1 second
                time.sleep(1.0)
except KeyboardInterrupt:
    GPIO.cleanup()
