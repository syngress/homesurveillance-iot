#!/usr/bin/env python

import RPi.GPIO as GPIO
import picamera
import subprocess
import time
import signal
import json

cmd = "/usr/bin/raspivid -t 5000 -o video.h264"
proc = subprocess.Popen(cmd.split(), shell=False)
