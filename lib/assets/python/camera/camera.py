#!/usr/bin/env python
import picamera
import subprocess
import time
import signal

cmd = "/usr/bin/raspistill --width 1024 --height 768 --quality 100 --timeout 1 --sharpness 0 --contrast 0 --saturation 0 --ev 0 --exposure auto --nopreview --output image.jpg"
proc = subprocess.Popen(cmd.split(), shell=False)
time.sleep(3.0)
proc.send_signal(signal.SIGUSR1)
proc.terminate()
