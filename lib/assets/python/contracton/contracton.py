#!/usr/bin/env python
#!/usr/bin/python
import time
import RPi.GPIO as gpio
import requests
import json

gpio.setmode(gpio.BCM)
door_pin = 23
gpio.setup(door_pin, gpio.IN, pull_up_down=gpio.PUD_UP)  # activate input with Pull-Up

while True:
    if gpio.input(door_pin):
        print("DOOR OPEN")
        # Send Email
        url = 'http://127.0.0.1:3000/contractons'
        payload = { 'target': 'Main Entrance', 'notification': True }
        headers = { 'content-type': 'application/json' }
        r = requests.post(url, data=json.dumps(payload), headers=headers)
        time.sleep(3.0)
    else:
        print("DOOR CLOSED")
    time.sleep(1)
