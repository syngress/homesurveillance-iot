import json, sys
import subprocess
import os
import os.path
import yaml
with open('config/settings.local.yml', 'r') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

services = config["systems"]["systemd_services"]

def main():
    data = {}
    for service in services:
        process = subprocess.Popen(["systemctl", "is-active",  service], stdout=subprocess.PIPE)
        (output, err) = process.communicate()
        output = output.decode('utf-8').rstrip("\n\r")
        data.update({service : {'status': str(output)}})

    print >> sys.stdout, data

main()
