# frozen_string_literal: true

module Middleware
  # lib/middleware/sidekiq_client.rb
  class SidekiqClient
    def call(_worker, items, _queue, _redis_pool = nil)
      items[:request_id] = Thread.current[:request_id] if Thread.current[:request_id].present?
      yield
    end
  end
end
