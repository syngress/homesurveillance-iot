# frozen_string_literal: true

module Middleware
  # lib/middleware/sidekiq_server.rb
  class SidekiqServer
    def call(_worker, items, _queue)
      Thread.current[:request_id] = items['request_id']
      yield
    end
  end
end
