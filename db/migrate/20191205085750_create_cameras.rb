# frozen_string_literal: true

# db/migrate/20191205085750_create_cameras.rb
class CreateCameras < ActiveRecord::Migration[5.1]
  def change
    create_table :cameras do |t|
      t.string :media_type
      t.integer :width
      t.integer :height
      t.integer :quality
      t.integer :timeout
      t.integer :sharpness
      t.integer :contrast
      t.integer :brightness
      t.integer :saturation
      t.integer :ev_compensation
      t.string :exposure
      t.integer :video_length

      t.timestamps
    end
  end
end
