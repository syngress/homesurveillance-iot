# frozen_string_literal: true

# db/migrate/20201129170427_create_temperatures.rb
class CreateTemperatures < ActiveRecord::Migration[5.1]
  def change
    create_table :temperatures do |t|
      t.decimal :temperature_celsius, precision: 10, scale: 2
      t.decimal :temperature_fahrenheit, precision: 10, scale: 2
      t.decimal :humidity, precision: 10, scale: 2
      t.boolean :notification

      t.timestamps
    end
  end
end
