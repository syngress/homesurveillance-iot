# frozen_string_literal: true

# db/migrate/20191221214610_create_contractons.rb
class CreateContractons < ActiveRecord::Migration[5.1]
  def change
    create_table :contractons do |t|
      t.string :target
      t.boolean :notification

      t.timestamps
    end
  end
end
