# frozen_string_literal: true

# db/migrate/20191118162324_create_systems.rb
class CreateSystems < ActiveRecord::Migration[5.1]
  def change
    create_table :systems do |t|
      t.boolean :operating
      t.integer :motion_interval
      t.boolean :camera_status
      t.boolean :mail_notification
      t.boolean :sms_notification
      t.boolean :door_sensor
      t.boolean :temperature_sensor
      t.integer :temperature_check_interval

      t.timestamps
    end
  end
end
