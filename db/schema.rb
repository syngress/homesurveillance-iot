# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_201_129_170_427) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'cameras', force: :cascade do |t|
    t.string 'media_type'
    t.integer 'width'
    t.integer 'height'
    t.integer 'quality'
    t.integer 'timeout'
    t.integer 'sharpness'
    t.integer 'contrast'
    t.integer 'brightness'
    t.integer 'saturation'
    t.integer 'ev_compensation'
    t.string 'exposure'
    t.integer 'video_length'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'contractons', force: :cascade do |t|
    t.string 'target'
    t.boolean 'notification'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'systems', force: :cascade do |t|
    t.boolean 'operating'
    t.integer 'motion_interval'
    t.boolean 'camera_status'
    t.boolean 'mail_notification'
    t.boolean 'sms_notification'
    t.boolean 'door_sensor'
    t.boolean 'temperature_sensor'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'temperatures', force: :cascade do |t|
    t.decimal 'temperature_celsius', precision: 10, scale: 2
    t.decimal 'temperature_fahrenheit', precision: 10, scale: 2
    t.decimal 'humidity', precision: 10, scale: 2
    t.boolean 'notification'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end
end
