# frozen_string_literal: true

module Contractons
  # app/params_filters/contractons/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id target notification]
      end
    end
  end
end
