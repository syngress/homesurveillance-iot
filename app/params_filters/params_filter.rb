# frozen_string_literal: true

# app/params_filters/params_filter.rb
module ParamsFilter
  def call(params)
    params = params.compact
    params.each { |param| check_param_value!(param) } if respond_to? :allowed_values
    params.select { |k, _v| k.to_sym.in? allowed_attributes }.try(:symbolize_keys)
  end

  def check_param_value!(param)
    key = param[0].to_sym
    val = param[1].to_sym

    case key
    when :sort_by
      raise Error::InvalidParameter.new key, value: val unless val.to_sym.in? allowed_values
    when :sort_order
      raise Error::InvalidParameter.new key, value: val unless val.in? %i[asc desc]
    end
  end
end
