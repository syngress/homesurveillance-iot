# frozen_string_literal: true

module Cameras
  # app/params_filters/cameras/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id width height quality sharpness contrast brightness saturation ev_compensation exposure type video_length]
      end
    end
  end
end
