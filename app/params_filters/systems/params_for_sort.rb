# frozen_string_literal: true

module Systems
  # app/params_filters/systems/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id operating motion_interval camera_status mail_notification sms_notification door_sensor]
      end
    end
  end
end
