# frozen_string_literal: true

module Systems
  # /app/serializers/systems/system_serializer.rb
  class SystemSerializer < ActiveModel::Serializer
    attributes %i[id operating motion_interval camera_status mail_notification sms_notification
                  door_sensor temperature_sensor temperature_check_interval]
  end
end
