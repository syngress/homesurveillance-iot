# frozen_string_literal: true

module Contractons
  # /app/serializers/contractons/contracton_serializer.rb
  class ContractonSerializer < ActiveModel::Serializer
    attributes %i[id target notification created_at]
  end
end
