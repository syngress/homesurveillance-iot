# frozen_string_literal: true

module Temperatures
  # app/serializers/temperatures/temperature_serializer.rb
  class TemperatureSerializer < ActiveModel::Serializer
    attributes %i[id temperature_celsius temperature_fahrenheit humidity notification created_at]
  end
end
