# frozen_string_literal: true

module Cameras
  # /app/serializers/cameras/camera_serializer.rb
  class CameraSerializer < ActiveModel::Serializer
    attributes %i[id width height quality timeout sharpness contrast brightness saturation
                  ev_compensation exposure media_type video_length created_at]
  end
end
