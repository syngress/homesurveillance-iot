# frozen_string_literal: true

module Systems
  # app/forms/systems/create_system_form.rb
  class CreateSystemForm < Form
    attribute :operating, Boolean, default: false
    attribute :motion_interval, Integer
    attribute :camera_status, Boolean
    attribute :mail_notification, Boolean
    attribute :sms_notification, Boolean
    attribute :door_sensor, Boolean
    attribute :temperature_sensor, Boolean
    attribute :temperature_check_interval, Integer

    validates :motion_interval,
              presence: true

    validates :camera_status,
              presence: true

    validates :mail_notification,
              presence: true

    validates :sms_notification,
              presence: true

    validates :door_sensor,
              presence: true

    validates :temperature_sensor,
              presence: true

    validates :temperature_check_interval,
              presence: true
  end
end
