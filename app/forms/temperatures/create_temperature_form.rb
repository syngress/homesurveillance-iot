# frozen_string_literal: true

module Temperatures
  # app/forms/temperatures/create_temperature_form.rb
  class CreateTemperatureForm < Form
    attribute :temperature_celsius, Decimal
    attribute :temperature_fahrenheit, Decimal
    attribute :humidity, Decimal
    attribute :notification, Boolean

    validates :temperature_celsius,
              presence: true

    validates :temperature_fahrenheit,
              presence: true

    validates :humidity,
              presence: true

    validates :notification,
              presence: true
  end
end
