# frozen_string_literal: true

module Contractons
  # app/forms/contractons/create_contracton_form.rb
  class CreateContractonForm < Form
    attribute :target, String
    attribute :notification, Boolean

    validates :target,
              presence: true,
              length: { maximum: 255 }

    validates :notification,
              presence: true
  end
end
