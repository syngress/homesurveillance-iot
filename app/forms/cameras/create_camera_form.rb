# frozen_string_literal: true

module Cameras
  # app/forms/cameras/create_camera_form.rb
  class CreateCameraForm < Form
    attribute :width, Integer, default: 1024
    attribute :height, Integer, default: 768
    attribute :quality, Integer, default: 75
    attribute :timeout, Integer, default: 1
    attribute :sharpness, Integer, default: 0
    attribute :contrast, Integer, default: 0
    attribute :brightness, Integer, default: 0
    attribute :saturation, Integer, default: 0
    attribute :ev_compensation, Integer, default: 0
    attribute :exposure, String, default: 'auto'
    attribute :media_type, String, default: 'photo'
    attribute :video_length, Integer, default: 5000 # miliseconds

    validates :width,
              presence: false,
              numericality: { only_integer: true }

    validates :height,
              presence: false,
              numericality: { only_integer: true }

    validates :quality,
              presence: false,
              numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than: 100 }

    validates :timeout,
              presence: false,
              numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than: 5 }

    validates :sharpness,
              presence: false,
              numericality: { only_integer: true, greater_than_or_equal_to: -100, less_than: 100 }

    validates :contrast,
              presence: false,
              numericality: { only_integer: true, greater_than_or_equal_to: -100, less_than: 100 }

    validates :brightness, # 0 is white 100 is black
              presence: false,
              numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than: 100 }

    validates :saturation,
              presence: false,
              numericality: { only_integer: true, greater_than_or_equal_to: -100, less_than: 100 }

    validates :ev_compensation,
              presence: false,
              numericality: { only_integer: true, greater_than_or_equal_to: -10, less_than: 10 }

    validates :exposure,
              presence: false,
              inclusion: { in: %w[auto night nightpreview backlight spotlight sports snow beach verylong fixedfps antishake fireworks] }

    validates :video_length, # Demo mode for raspivideo, time in miliseconds
              presence: false,
              numericality: { only_integer: true, greater_than_or_equal_to: 5000, less_than: 6_000_0 }

    validates :media_type,
              presence: false,
              inclusion: { in: %w[photo video] }
  end
end
