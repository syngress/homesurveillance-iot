# frozen_string_literal: true

# /app/forms/form.rb
class Form
  include ActiveModel::Validations
  include Virtus.model
end
