# frozen_string_literal: true

# /app/controllers/application_controller.rb
class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler
  include ErrorHandler

  before_action :set_locale

  def set_locale
    I18n.locale = params[:lang] || Settings.locale_default_lang
    raise Error::InvalidParameter.new(:lang, value: params[:lang]) if params[:lang] == 'en'
  rescue I18n::InvalidLocale
    raise Error::InvalidParameter.new(:lang, value: params[:lang])
  end
end
