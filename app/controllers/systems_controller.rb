# frozen_string_literal: true

# /app/controllers/system_controller.rb
class SystemsController < ApplicationController
  include Pagination
  include Sorting

  before_action :system_object, only: [:show]

  def show
    render json: system_object,
           serializer: Systems::SystemSerializer
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: { recordsTotal: systems.count,
                   data: systems },
           each_serializer: Systems::SystemSerializer
  end

  def create
    form = Systems::CreateSystemForm.new(system_params)
    result = Systems::CreateSystemService.call(form: form)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Systems::SystemSerializer,
             status: :created
    end
  end

  def check_system_services
    result = Systems::CheckSystemService.call
    render json: { services: result }
  end

  private

  def system_params
    params.permit(
      :operating,
      :motion_interval,
      :camera_status,
      :mail_notification,
      :sms_notification,
      :door_sensor,
      :temperature_sensor,
      :temperature_check_interval
    ).to_h
  end

  def systems
    @systems ||= Systems::GetSystemService.call(
      params: params,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def system_object
    @system_object = System.find(params[:id])
  end

  def sort_by_parameter
    Systems::ParamsForSort.allowed_values
  end
end
