# frozen_string_literal: true

# app/controllers/contractons_controller.rb
class ContractonsController < ApplicationController
  include Pagination
  include Sorting

  before_action :contracton, only: [:show]

  def show
    render json: contracton,
           serializer: Contractons::ContractonSerializer
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: { recordsTotal: contractons.count,
                   data: contractons },
           each_serializer: Contractons::ContractonSerializer
  end

  def create
    form = Contractons::CreateContractonForm.new(contracton_params)
    result = Contractons::CreateContractonService.call(form: form)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Contractons::ContractonSerializer,
             status: :created
    end
  end

  private

  def contracton_params
    params.permit(:target, :notification).to_h
  end

  def contractons
    @contractons ||= Contractons::GetContractonService.call(params: params, page: page, per_page: per_page, sort_by: sort_by, sort_order: sort_order)
  end

  def contracton
    @contracton = Contracton.find(params[:id])
  end

  def sort_by_parameter
    Contractons::ParamsForSort.allowed_values
  end
end
