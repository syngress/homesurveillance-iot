# frozen_string_literal: true

# app/controllers/cameras_controller.rb
class CamerasController < ApplicationController
  include Pagination
  include Sorting

  before_action :camera, only: [:show]

  def show
    render json: camera,
           serializer: Cameras::CameraSerializer
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: { recordsTotal: cameras.count,
                   data: cameras },
           each_serializer: Cameras::CameraSerializer
  end

  def create
    form = Cameras::CreateCameraForm.new(camera_params)
    result = Cameras::CreateCameraService.call(form: form)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Cameras::CameraSerializer,
             status: :created
    end
  end

  private

  def camera_params
    params.permit(
      :width,
      :height,
      :quality,
      :timeout,
      :sharpness,
      :contrast,
      :brightness,
      :saturation,
      :ev_compensation,
      :exposure,
      :media_type,
      :video_length
    ).to_h
  end

  def cameras
    @cameras ||= Cameras::GetCameraService.call(
      params: params,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def camera
    @camera = Camera.find(params[:id])
  end

  def sort_by_parameter
    Cameras::ParamsForSort.allowed_values
  end
end
