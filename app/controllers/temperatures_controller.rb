# frozen_string_literal: true

# app/controllers/temperatures_controller.rb
class TemperaturesController < ApplicationController
  include Pagination
  include Sorting

  before_action :temperature, only: [:show]

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: { recordsTotal: temperatures.count,
                   data: temperatures },
           each_serializer: Temperatures::TemperatureSerializer
  end

  def create
    form = Temperatures::CreateTemperatureForm.new(temperature_params)
    result = Temperatures::CreateTemperatureService.call(form: form)
    if result.error?
      render_error(result.object)
    else
      render json: result.object,
             serializer: Temperatures::TemperatureSerializer,
             status: :created
    end
  end

  private

  def temperature_params
    params.permit(:temperature_celsius, :temperature_fahrenheit, :humidity, :notification).to_h
  end

  def temperatures
    @temperatures ||= Temperatures::GetTemperatureService.call(
      params: params,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def temperature
    @temperature = Temperature.find(params[:id])
  end
end
