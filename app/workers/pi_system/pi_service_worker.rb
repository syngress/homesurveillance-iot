# frozen_string_literal: true

module PiSystem
  # app/workers/pi_system/pi_service_worker.rb
  class PiServiceWorker
    include Sidekiq::Worker

    def perform(service_status_check)
      system "python lib/assets/python/email/send_inactive_service_mail.py #{service_status_check}"
    end
  end
end
