# frozen_string_literal: true

module PiCamera
  # app/workers/pi_camera/pi_camera_worker.rb
  class PiCameraWorker
    include Sidekiq::Worker

    def perform(media_type)
      photo if media_type == 'photo'
      video if media_type == 'video'
    end

    def photo
      python_script = 'camera.py'
      file = 'image.jpg'
      create_and_send(python_script, file)
    end

    def video
      python_script = 'video.py'
      file = 'video.h264'
      create_and_send(python_script, file)
    end

    def create_and_send(python_script, file)
      system 'python lib/assets/python/camera/'"#{python_script}"''
    ensure
      system 'python lib/assets/python/email/send_mail.py '"#{file}"''
      File.delete(file.to_s) if File.exist?(file.to_s)
    end
  end
end
