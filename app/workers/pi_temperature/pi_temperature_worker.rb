# frozen_string_literal: true

module PiTemperature
  # app/workers/pi_temperature/pi_temperature_worker.rb
  class PiTemperatureWorker
    include Sidekiq::Worker

    def perform(notification_enabled)
      create_and_send if notification_enabled
    end

    def create_and_send
      system 'python lib/assets/python/email/send_temperature_mail.py'
    rescue StandardError => e
      puts "Rescued: #{e.inspect}"
    end
  end
end
