# frozen_string_literal: true

class Worker
  include Sidekiq::Worker
  sidekiq_options backtrace: 10, retry: 10
end
