# frozen_string_literal: true

module CronWorker
  # app/workers/cron_worker/cron_service_worker.rb
  class CronServiceWorker < Worker
    def perform(_attrs = {})
      Systems::CheckSystemService.call if Settings.systems.check_service_notification
    end
  end
end
