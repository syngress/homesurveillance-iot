# frozen_string_literal: true

class Error
  # /app/models/error/resource_not_found.rb
  class ResourceNotFound < Error
    def initialize(message = nil)
      message ||= I18n.t :resource_not_found, scope: 'locale.messages' unless message
      super(message, :resource_not_found, {}, Http::Status.new(404))
    end
  end
end
