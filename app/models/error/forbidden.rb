# frozen_string_literal: true

class Error
  # /app/models/error/forbidden.rb
  class Forbidden < Error
    def initialize(message = nil)
      message ||= I18n.t :forbidden, scope: 'locale.messages' unless message
      super(message, :forbidden, {}, Http::Status.new(403))
    end
  end
end
