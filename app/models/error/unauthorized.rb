# frozen_string_literal: true

class Error
  # /app/models/error/unauthorized.rb
  class Unauthorized < Error
    def initialize(message = nil)
      message ||= I18n.t :token_invalid, scope: 'locale.messages' unless message
      super(message, :token_invalid, {}, Http::Status.new(401))
    end
  end
end
