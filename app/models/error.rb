# frozen_string_literal: true

# app/models/error.rb
class Error < StandardError
  include ActiveModel::Serialization

  attr_reader :key, :details, :http_status

  def initialize(message, key, details = {}, http_status = nil)
    raise ArgumentError, 'Message of the error cannot be blank' if message.blank?
    raise ArgumentError, 'Key of the error cannot be nil' if key.blank?

    raise_argument_error(I18n.t(:http_status_instance, scope: 'errors.messages', http_status: Http::Status)) unless http_status.nil? || http_status.is_a?(Http::Status)

    super(message)
    @key = key.to_sym
    @details = details
    @http_status = http_status
  end

  def as_json(_args)
    {
      status: http_status.status,
      error: key,
      message: message,
      details: details
    }
  end

  def raise_argument_error(message)
    raise ArgumentError message
  end
end
