# frozen_string_literal: true

module Cameras
  # /app/services/cameras/create_camera_service.rb
  class CreateCameraService
    include Service

    attr_reader :form

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
    end

    def call
      return build_result(validation_error) unless form.valid?

      PiCamera::PiCameraWorker.perform_async(form.media_type)
      build_result created_object
    end

    def created_object
      Camera.create!(@form.to_hash)
    end
  end
end
