# frozen_string_literal: true

module Cameras
  # /app/services/cameras/get_camera_service.rb
  class GetCameraService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
    end

    def call
      receive_camera
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :params

    def receive_camera
      cameras.empty? ? (raise Error::ResourceNotFound) : cameras
    end

    def cameras
      @cameras ||= Camera.all.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
