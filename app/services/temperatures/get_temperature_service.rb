# frozen_string_literal: true

module Temperatures
  # app/services/temperatures/get_temperature_service.rb
  class GetTemperatureService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
    end

    def call
      receive_temperatures
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page

    def receive_temperatures
      temperatures.empty? ? (raise Error::ResourceNotFound) : temperatures
    end

    def temperatures
      @temperatures ||= Temperature.all.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
