# frozen_string_literal: true

module Temperatures
  # app/services/temperatures/create_temperature_service.rb
  class CreateTemperatureService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
    end

    def call
      return build_result(validation_error) unless form.valid?

      PiTemperature::PiTemperatureWorker.perform_async(form.notification)
      build_result created_object
    end

    def created_object
      Temperature.create!(@form.to_hash)
    end
  end
end
