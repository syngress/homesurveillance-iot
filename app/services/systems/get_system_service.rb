# frozen_string_literal: true

module Systems
  # /app/services/systems/get_system_service.rb
  class GetSystemService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
    end

    def call
      receive_systems
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :params, :contractons

    def receive_systems
      systems.empty? ? (raise Error::ResourceNotFound) : systems
    end

    def systems
      @systems ||= System.all.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
