# frozen_string_literal: true

module Systems
  # /app/services/systems/check_system_service.rb
  class CheckSystemService
    include Service
    require 'open3'

    def initialize(attrs = {}); end

    def call
      send_email_notification
      return_systemctl_data
    end

    private

    # Send proper email notification to defined recipients
    def send_email_notification
      return unless Settings.systems.check_service_notification

      raise Error::ValidationFailed (I18n.t :systemd_not_defined, scope: 'errors.messages') unless Settings.systems.systemd_services.present?

      PiSystem::PiServiceWorker.perform_async(service_status_check)
    end

    # Format and return data to controller if inactive service present
    def return_systemctl_data
      services[0].present? ? @return_systemctl_data ||= YAML.safe_load(JSON.parse(services[0].to_json)) : []
    end

    # Receive services from OS defined in application configuration
    def receive_systemctl_data
      data = []
      _stdin, stdout, _stderr = Open3.popen3('python lib/assets/python/system/systemd.py')
      stdout.each do |service|
        data << service.strip
      end

      data
    end

    # Catch inactive services
    def service_status_check
      inactive_services = []

      return_systemctl_data&.each do |service_data|
        inactive_services << service_data[0] if service_data[1].values.first != 'active'
      end

      inactive_services
    end

    def services
      @services ||= receive_systemctl_data
    end
  end
end
