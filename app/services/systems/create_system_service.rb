# frozen_string_literal: true

module Systems
  # /app/services/systems/create_system_service.rb
  class CreateSystemService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
    end

    def call
      return build_result(validation_error) unless form.valid?

      cleanup_screen
      create_object
      form.operating ? enable_system : disable_system
      build_result create_object
    end

    def create_object
      @create_object ||= System.create!(@form.to_hash)
    end

    def cleanup_screen
      system 'killall screen'
    end

    def enable_system
      enable_motion_sensor
      enable_contracton_sensor if form.door_sensor
      enable_temperature_sensor if form.temperature_sensor
    end

    def enable_motion_sensor
      system "screen -S motion -dm bash -c 'python lib/assets/python/motion/motion.py'"
      sleep 2.0
    end

    def enable_contracton_sensor
      system "screen -S contracton -dm bash -c 'python lib/assets/python/contracton/contracton.py'"
      sleep 2.0
    end

    def enable_temperature_sensor
      system "screen -S temperature -dm bash -c 'python lib/assets/python/temperature/temperature.py'"
      sleep 2.0
    end

    def disable_system
      system 'killall screen'
    end
  end
end
