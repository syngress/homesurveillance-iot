# frozen_string_literal: true

module Contractons
  # /app/services/contractons/get_contracton_service.rb
  class GetContractonService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
    end

    def call
      receive_contractons
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :params

    def receive_contractons
      contractons.empty? ? (raise Error::ResourceNotFound) : contractons
    end

    def contractons
      @contractons ||= Contracton.all.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
