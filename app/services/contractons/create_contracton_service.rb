# frozen_string_literal: true

module Contractons
  # /app/services/contractons/create_contracton_service.rb
  class CreateContractonService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
    end

    def call
      return build_result(validation_error) unless form.valid?

      contracton_notification if form.notification == TRUE
      build_result created_object
    end

    def created_object
      Contracton.create!(@form.to_hash)
    end

    def contracton_notification
      system 'python lib/assets/python/email/send_door_mail.py'
    ensure
      sleep 1.5
    end
  end
end
