# frozen_string_literal: true

require_relative 'boot'
require 'rails/all'

Bundler.require(*Rails.groups)

module Survilance
  # config/application.rb
  class Application < Rails::Application
    config.web_console.whitelisted_ips = '192.168.0.0/16'
    config.load_defaults 5.1
    config.api_only = true
    config.i18n.default_locale = :pl_PL
    config.i18n.available_locales = %i[en en_GB pl_PL]
  end
end
