# frozen_string_literal: true

Rails.application.routes.draw do
  Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]
  Sidekiq::Web.set :sessions, Rails.application.config.session_options
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == Settings.basic_auth.username && password == Settings.basic_auth.password
  end
  mount Sidekiq::Web => '/sidekiq'
  resources :systems, only: %i[index show create]
  get '/services', to: 'systems#check_system_services', as: :systemd_services
  resources :cameras, only: %i[index show create]
  resources :contractons, only: %i[index show create]
  resources :temperatures, only: %i[index show create]
end
