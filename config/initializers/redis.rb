# frozen_string_literal: true

# INITIALIZE SIDEKIQ REDIS
redis_sentinel_config = {
  url: "redis://#{Settings.redis.master_name}",
  role: :master,
  db: Settings.redis.db,
  sentinels: (Settings.redis.sentinels.map(&:to_h) if Settings.redis.sentinels?)
}

redis_config = {
  host: Settings.redis.host,
  port: Settings.redis.port,
  db: Settings.redis.db
}

REDIS_SETTINGS = Settings.redis.sentinels? ? redis_sentinel_config : redis_config
REDIS = Redis.new(REDIS_SETTINGS)

# INITIALIZE CACHE
Rails.application.config.cache_store = :redis_store, REDIS_SETTINGS.merge(namespace: :cache, expires_in: Settings.cache.default.expires_in)
Rails.cache = ActiveSupport::Cache.lookup_store(Rails.application.config.cache_store)
