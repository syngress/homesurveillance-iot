# frozen_string_literal: true

require 'middleware/sidekiq_client'
require 'middleware/sidekiq_server'
require 'sidekiq/web'
require 'sidekiq/cron/web'

Sidekiq::Web.set :session_secret, Settings.secret_key_base
Sidekiq::Web.set :sessions, Rails.application.config.session_options

Sidekiq.configure_client do |config|
  config.redis = REDIS_SETTINGS
  config.client_middleware do |chain|
    chain.add Middleware::SidekiqClient
  end
end

Sidekiq.configure_server do |config|
  config.redis = REDIS_SETTINGS
  config.client_middleware do |chain|
    chain.add Middleware::SidekiqClient
  end
  config.server_middleware do |chain|
    chain.add Middleware::SidekiqServer
    Sidekiq::Cron::Job.load_from_hash(YAML.load_file('config/schedule.yml'))
  end
end

Sidekiq.logger = nil if Rails.env.production?
